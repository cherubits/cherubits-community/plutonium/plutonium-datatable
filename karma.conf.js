/* eslint-disable import/no-extraneous-dependencies */
const { createDefaultConfig } = require('@open-wc/testing-karma');
const merge = require('deepmerge');

process.env.CHROME_BIN = require('puppeteer').executablePath();

module.exports = config => {
  config.set(
    merge(createDefaultConfig(config), {
      files: [
        // runs all files ending with .test in the test folder,
        // can be overwritten by passing a --grep flag. examples:
        //
        // npm run test -- --grep test/foo/bar.test.js
        // npm run test -- --grep test/bar/*
        {
          pattern: config.grep ? config.grep : 'dist/**/test/**/*.test.js',
          type: 'module',
        },
      ],
      plugins: ['karma-junit-reporter', 'karma-coverage'],
      esm: {
        nodeResolve: true,
      },
      // you can overwrite/extend the config further
      singleRun: true,
      browsers: ['ChromeHeadless'],
      reporters: ['progress', 'junit', 'coverage'],
      junitReporter: {
        outputDir: 'test-results',
        outputFile: 'test-report.xml',
        useBrowserName: true,
        suite: '',
      },
      coverageReporter: {
        dir: 'coverage',
        watermarks: {
          statements: [50, 75],
          functions: [50, 75],
          branches: [50, 75],
          lines: [50, 75],
        },
        reporters: [
          {
            type: 'text',
            // enforce percentage thresholds
            // anything under these percentages will cause karma to fail with an exit code of 1 if not running in watch mode
            check: {
              global: {
                statements: 0,
                branches: 0,
                functions: 0,
                lines: 0,
                // excludes: [
                //   'foo/bar/**/*.js'
                // ]
              },
              each: {
                statements: 0,
                branches: 0,
                functions: 0,
                lines: 0,
                // excludes: [
                //   'other/directory/**/*.js'
                // ],
                // overrides: {
                //   'baz/component/**/*.js': {
                //     statements: 98
                //   }
                // }
              },
            },
          },
          {
            type: 'cobertura',
            file: 'coverage.xml',
            subdir: '.',
            // enforce percentage thresholds
            // anything under these percentages will cause karma to fail with an exit code of 1 if not running in watch mode
            check: {
              global: {
                statements: 50,
                branches: 50,
                functions: 50,
                lines: 50,
                // excludes: [
                //   'foo/bar/**/*.js'
                // ]
              },
              each: {
                statements: 50,
                branches: 50,
                functions: 50,
                lines: 50,
                // excludes: [
                //   'other/directory/**/*.js'
                // ],
                // overrides: {
                //   'baz/component/**/*.js': {
                //     statements: 98
                //   }
                // }
              },
            },
          },
        ],
        check: {
          global: {
            statements: 50,
            branches: 50,
            functions: 50,
            lines: 50,
          },
          each: {
            statements: 50,
            branches: 50,
            functions: 50,
            lines: 50,
          },
        },
      },
    })
  );
  return config;
};
